# Licenses

## Everything except the files in the following headlines

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

## `users/default/sway/color-picker.sh`

[MIT License](https://github.com/jgmdev/wl-color-picker/blob/main/LICENSE)

Copyright (c) 2021 Jefferson González

## `backgrounds/highway-to-shell.png`

© 2013 Helena Mosher  
© 2013 kernel concepts  
© 2013 LinuxTag e.V.  

- title: *Highway to Shell*

- licensed under [CC BY-NC-ND 3.0](https://creativecommons.org/licenses/by-nc-nd/3.0/legalcode)

- no changes were made

- for more information visit [LinuxTag: HIGHWAY TO SHELL: OFFIZIELLES LINUXTAG-T-SHIRT UNTER CC FREIGEGEBEN](http://www.linuxtag.org/2013/de/presse/newsarchiv/entry/article/highway-to-shell-offizielles-linuxtag-t-shirt-rockt-so-sehr-dass-es-cc-wird.html)